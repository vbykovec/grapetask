class RenameFieldPubdate < ActiveRecord::Migration
  def change
    rename_column :news, :pubdate, :pubDate
  end
end
