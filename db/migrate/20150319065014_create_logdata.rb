class CreateLogdata < ActiveRecord::Migration
  def change
    create_table :logdata do |t|
      t.text :data

      t.timestamps null: false
    end
  end
end
