class AddPubDateToNewsAsDate < ActiveRecord::Migration
  def change
   add_column :news, :pubDate, :date
  end
end
