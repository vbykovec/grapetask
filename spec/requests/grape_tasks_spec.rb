require 'rails_helper'

RSpec.describe "GrapeTasks", type: :request do
  describe "GET /v1/get_news" do
    it "return items" do
      get '/v1/get_news'
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to be_a_kind_of(Array)
    end

    it "return 10 items max" do
      20.times do
        News.create(pubDate: Date.current, title: 'test')
      end
      get '/v1/get_news'
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to be_a_kind_of(Array)
      expect(JSON.parse(response.body).count).to eq 10
    end
  end

  describe "PUT /v1/update_log" do
    it "update log" do
      put '/v1/update_log', id: 1, value1: 'abcd'
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to eq({'action' => 'put'})
    end

    it "not update log without id" do
      put '/v1/update_log', value1: 'abcd'
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq({'error' => 'id is missing'})
    end

    it "not update log without any value" do
      put '/v1/update_log', id: '11'
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to have_key 'error'
    end
    
    it "should not update log with invalid value2" do
      put '/v1/update_log', id: '11', value2: '____'
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to have_key 'error'
    end

    it "should not update log with invalid id" do
      put '/v1/update_log', id: 'ab', value2: '____'
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to have_key 'error'
    end
  end

  describe "POST /v1/create_log" do
    it "create log" do
      post '/v1/create_log', value1: 'abcd', value2: 'rstt'
      expect(response).to have_http_status(201)
      expect(JSON.parse(response.body)).to eq({'action' => 'post'})
    end

    it "not create log with invalid values" do
      post '/v1/create_log', value1: 'ab!d', value2: 'rs!t'
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to have_key('error')
    end

    it "not create log with id passed" do
      post '/v1/create_log', id: '1', value2: 'rs!t'
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to have_key('error')
    end
  end

  describe "DELETE /v1/delete_log" do
    it "delete log" do
      delete '/v1/delete_log', id: 1
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to eq({'action' => 'delete'})
    end

    it "not delete log without id" do
      delete '/v1/delete_log'
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to have_key 'error'
    end

    it "not delete log with invalid id" do
      delete '/v1/delete_log', id: 'a'
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to have_key 'error'
    end
  end
end
