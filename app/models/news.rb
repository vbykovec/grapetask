require 'open-uri'
require 'contracts'
class News < ActiveRecord::Base
  DATA_URL = 'http://rubyweekly.com/rss' # URL of rss for models
  include Contracts
  
  Contract None => ArrayOf[Hash]
  def self.fetch_latest_ten
    unless News.where('created_at >= ?',8.hours.ago).any?
      doc = Nokogiri::XML(open(DATA_URL))
      doc.css('item').each do |item|
        data = {}
        %w(title description guid link pubDate).each do |field|
          data[field] = item.css(field).text()
        end
        next if data['guid'].blank?
        data['pubDate'] = Date.parse(data['pubDate'])
        new_item = News.find_or_create_by data
      end
    end
    News.order('"pubDate" desc').limit(10).map &:attributes
  end
end
