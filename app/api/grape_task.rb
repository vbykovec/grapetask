module GrapeTask
  class APIv1 < Grape::API
    version 'v1', using: :path, vendor: :grapetask
    format :json
    default_format :json

    helpers do
      def logger
        APIv1.logger
      end
    end

    get :get_news do
      News.fetch_latest_ten
    end


    desc 'update log'
    params do
      requires :id, type: Integer
      optional :value1, type: String, regexp: /^[a-zA-Z0-9]+$/
      optional :value2, type: String, regexp: /^[a-zA-Z0-9]+$/
      optional :value3, type: String, regexp: /^[a-zA-Z0-9]+$/
      at_least_one_of :value1, :value2, :value3
    end
    put :update_log do
      logger.info "PUT #{params}"
      {action: 'put'}
    end
      

    desc 'create log'
    params do
      optional :value1, type: String, regexp: /^[a-zA-Z0-9]+$/
      optional :value2, type: String, regexp: /^[a-zA-Z0-9]+$/
      optional :value3, type: String, regexp: /^[a-zA-Z0-9]+$/
      at_least_one_of :value1, :value2, :value3
    end
    post :create_log do
      logger.info "POST #{params}"
      {action: 'post'}
    end

    desc 'delete log'
    params do
      requires :id, type: Integer
    end
    delete :delete_log do
      logger.info "DELETE #{params}"
      {action: 'delete'}
    end
  end
end
